const execa = require('execa')

module.exports = async (pluginConfig, { nextRelease: { version }, logger }) => {
  logger.log(
    `Pushing registry.gitlab.com/${
      pluginConfig.name
    }:latest and registry.gitlab.com/${
      pluginConfig.name
    }:${version} to Gitlab registry`
  )

  // Push both new version and latest
  execa('docker', ['push', `registry.gitlab.com/${pluginConfig.name}:latest`], {
    stdio: 'inherit',
  })
  execa(
    'docker',
    [
      'tag',
      `registry.gitlab.com/${pluginConfig.name}:latest`,
      `registry.gitlab.com/${pluginConfig.name}:${version}`,
    ],
    { stdio: 'inherit' }
  )
  execa(
    'docker',
    ['push', `registry.gitlab.com/${pluginConfig.name}:${version}`],
    { stdio: 'inherit' }
  )
}
