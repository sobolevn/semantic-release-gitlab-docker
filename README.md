# semantic-release-gitlab-docker

[![npm](https://img.shields.io/npm/v/semantic-release-gitlab-docker.svg)](https://www.npmjs.com/package/jeffthefate/semantic-release-gitlab-docker)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![license](https://img.shields.io/npm/l/semantic-release-gitlab-docker.svg)](https://gitlab.com/foxfarmroad/semantic-release-gitlab-docker/blob/master/LICENSE) -->

Set of [semantic-release](https://github.com/semantic-release/semantic-release) plugins for publishing a docker image to
[Gitlab Registry](https://registry.gitlab.com/) from [Gitlab](https://www.gitlab.com) CI.

```json
{
  "release": {
    "verifyConditions": "semantic-release-gitlab-docker",
    "publish": {
      "path": "semantic-release-gitlab-docker",
      "name": "username/imagename"
    }
  }
}
```

## Configuration

Your Gitlab CI is configured automatically using `gitlab-ci-token` and `CI_BUILD_TOKEN` as the username and password for `registry.gitlab.com`.

In addition, you need to specify the name of the image as the `name` setting.

## Plugins

### `verifyConditions`

Verify that all needed configuration is present and login to the Gitlab registry.

### `publish`

Tag the image specified by `name` with the new version, push it to Gitlab registry and update the `latest` tag.

## Example .gitlab-ci.yml

```yml
image: registry.gitlab.com/foxfarmroad/ffr-docker-npm:latest

# When using dind, it's wise to use the overlayfs driver for
# improved performance.
variables:
  DOCKER_DRIVER: overlay2

services:
  - docker:dind

stages:
  - build

before_script:
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
  - npm install

build:
  stage: build
  script:
    - docker build -t registry.gitlab.com/foxfarmroad/ffr-gitlab-ci-node-serverless .
    - npx semantic-release@15.5.0
```
